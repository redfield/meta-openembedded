SUMMARY = "All packages required for a base install of Gnome Shell"
SECTION = "x11/wm"

inherit packagegroup

RDEPENDS_${PN} = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'packagegroup-core-x11', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'wayland', '', d)} \
    gnome-shell \
    gnome-session \
    gnome-bluetooth \
    upower \
    pulseaudio \
    gnome-settings-daemon \
"
