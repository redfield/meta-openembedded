DESCRIPTION = "GNOME Shell Extensions"
LICENSE = "GPLv2"
DEPENDS = " \
    glib-2.0 \
    gsettings-desktop-schemas \
    gjs \
    gnome-desktop3 \
    gdm \
    mutter \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland-protocols", "", d)} \
    evolution-data-server \
    libcroco \
    polkit \
    gnome-bluetooth \
    gstreamer1.0 \
    keybinder \
    gnome-settings-daemon \
    ibus \
    librsvg \
    python3 \
    sassc-native \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=4cb3a392cbf81a9e685ec13b88c4c101"

SRC_URI = " \
    http://http.debian.net/debian/pool/main/g/${BPN}/${BPN}_${PV}.orig.tar.xz \
"

SRC_URI[md5sum] = "624b7f986e4be2e2a990ea9ac92668e5"
SRC_URI[sha256sum] = "b7301209ff143f9e83d43f9521ab53c9f8db0104ce562e4ec8a49c5e1cb13dbf"

RDEPENDS_${PN} = " \
    gnome-shell \
"

EXTRA_OEMESON += " -Dextension_set=all "
EXTRA_OEMESON_remove = "-Dintrospection=true"

FILES_${PN} += "${datadir}"

inherit pkgconfig meson gobject-introspection gettext
