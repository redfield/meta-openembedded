DESCRIPTION = "Mutter (Gnome 3 window manager)"
LICENSE = "GPLv2"
DEPENDS = " \
    glib-2.0 \
    gsettings-desktop-schemas \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland-native", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland-protocols", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "virtual/egl", "", d)} \
    libxinerama \
    libinput \
    gtk+3 \
    gnome-desktop3 \
    libxcursor \
    libxkbfile \
    cogl-1.0 \
    clutter-1.0 \
"

RDEPENDS_${PN} += " \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "libdrm", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "libxcb", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "libgudev", "", d)} \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRC_URI = " \
    https://download.gnome.org/core/3.30/3.30.2/sources/${PN}-${PV}.tar.xz \
    file://0001-disable-zenity-check.patch \
    file://0002-fixup-introspection-variables-with-correct-path.patch \
    file://0003-packageconfig-variable-fixup.patch \
    file://0004-fix-broken-includes.patch \
    file://0001-EGL-include-EGL-eglmesaext.h.patch \
"

SRC_URI[md5sum] = "e02cc37b9dc7b8c668c2b326fdc29162"
SRC_URI[sha256sum] = "b5519969a3ff3005632f1de984ec00c4d2b3986d01c31c1a19acfc93dd0c9c24"

inherit autotools pkgconfig gobject-introspection gettext

export GIR_EXTRA_LIBS_PATH="${B}/cogl/cogl/.libs:${B}/cogl/cogl-pango/.libs:${B}/cogl/cogl-path/.libs:${B}/clutter/clutter/.libs"

do_configure_prepend() {
    # Autotools expects config.rpath to be with in the source tree
    install -d ${S}/build-aux
    cp ${STAGING_DIR_NATIVE}${datadir}/gettext/config.rpath ${S}/build-aux/config.rpath

    # There are several Werrors that trip when building with Wayland enabled:
    # -Wno-error=implicit-function-declaration -Wno-error=nested-externs -Wno-error=int-conversion
    # and setting those as no-error does not seem to get picked up (it does appear in the compilation
    # command
    sed -i 's^-Werror -Wno-error=deprecated-declarations^-Wno-error=deprecated-declarations^g' ${S}/configure.ac
}

do_install_append() {
    install -d ${D}/${datadir}/gir-1.0
    install ${B}/src/Meta-3.gir ${D}/${datadir}/gir-1.0
    install ${B}/cogl/cogl/Cogl-3.gir ${D}/${datadir}/gir-1.0
    install ${B}/cogl/cogl-pango/CoglPango-3.gir ${D}/${datadir}/gir-1.0
    install ${B}/clutter/clutter/Cally-3.gir ${D}/${datadir}/gir-1.0
    install ${B}/clutter/clutter/ClutterX11-3.gir ${D}/${datadir}/gir-1.0
    install ${B}/clutter/clutter/Clutter-3.gir ${D}/${datadir}/gir-1.0
}

FILES_${PN} += "${datadir}"

