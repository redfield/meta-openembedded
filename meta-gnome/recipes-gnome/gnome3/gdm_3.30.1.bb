DESCRIPTION = "GNOME Desktop Manager"
LICENSE = "GPLv2"
DEPENDS = " \
    accountsservice \
    dconf-native \
    gconf-native \
    glib-2.0 \
    gtk+3 \
    libcanberra \
    libpam \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRC_URI = " \
    https://download.gnome.org/sources/gdm/3.30/${PN}-${PV}.tar.xz \
    file://Xsession \
    file://install-pam-data.patch \
"

SRC_URI[md5sum] = "83094013e224a072f6adc086c034f076"
SRC_URI[sha256sum] = "4b3d11856adf9fc26b43b2742c196e9e9dc2d2a5eff8bb34d906537288e2732f"

inherit autotools pkgconfig gobject-introspection gettext systemd useradd

PACKAGECONFIG ?= " \
     ${@bb.utils.filter('DISTRO_FEATURES', 'pam', d)} \
     plymouth \
"

PACKAGECONFIG[plymouth] = "--with-plymouth,,plymouth"
PACKAGECONFIG[pam] = "--with-default-pam-config=openembedded,,libpam"

EXTRA_OECONF += " --with-default-pam-config=lfs "

do_install_append() {
	chown -R gdm:gdm ${D}${localstatedir}/lib/gdm

	# install Xsession
	install -m 0755 ${WORKDIR}/Xsession ${D}${sysconfdir}/gdm/Xsession

	# Remove installed volatiles.
	rm -rf ${D}${localstatedir}/run

	VOLATILE_DIRS=" \
		${localstatedir}/lib/gdm/.local/share/xorg \
	"

	if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
		install -d ${D}${sysconfdir}/tmpfiles.d
		for i in ${VOLATILE_DIRS}; do
			echo "d $i 0755 gdm gdm - -" >> ${D}${sysconfdir}/tmpfiles.d/gdm.conf
		done
	fi

	if ${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'true', 'false', d)}; then
		install -d ${D}${sysconfdir}/default/volatiles
		for i in ${VOLATILE_DIRS}; do
			echo "d gdm gdm 0755 $i none" >> ${D}${sysconfdir}/default/volatiles/99_gdm
		done
	fi

	# anyone installing gdm into their image most likely wants to run it by default
  cat >> ${D}${systemd_system_unitdir}/gdm.service <<EOF
WantedBy=multi-user.target
EOF
}

USERADD_PACKAGES = "${PN}"
USERADD_PARAM_${PN} = " \
    --system --home /var/lib/gdm \
    --no-create-home --shell /bin/false \
    --user-group gdm \
"

SYSTEMD_SERVICE_${PN} = "gdm.service"

FILES_${PN} += " \
    ${datadir} \
    ${libdir}/security/pam_gdm.so \
"

