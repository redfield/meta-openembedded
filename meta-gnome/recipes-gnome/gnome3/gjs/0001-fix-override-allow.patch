gjs: Fix broken override GInterface properties

  [gjs Ticket #186] -- https://gitlab.gnome.org/GNOME/gjs/issues/186
 
  Overriding introspected GObjectInterface properties 
  
  Signed-off-by: Philip Chimento <philip.chimento@gmail.com>
  
  The patch was imported from the gjs git server
  (git://gitlab.gnome.org/GNOME/gjs) with a commit id of
  322ffab6b0b24ab0ce36079642021d969b081730.

  Upstream-status: Backport
  
  This recipe can move to tracking a more appropriate revision
  once a recipe for building mozjs60 is created, and then this
  patch can be dropped.

  Signed-off-by: Brendan Kerrigan <kerriganb@ainfosec.com>
diff --git a/gi/object.cpp b/gi/object.cpp
index 2b2265da..70032736 100644
--- a/gi/object.cpp
+++ b/gi/object.cpp
@@ -717,7 +717,11 @@ bool ObjectPrototype::lazy_define_gobject_property(JSContext* cx,
     JS::RootedValue private_id(cx, JS::StringValue(JSID_TO_STRING(id)));
     if (!gjs_define_property_dynamic(
             cx, obj, name, "gobject_prop", &ObjectBase::prop_getter,
-            &ObjectBase::prop_setter, private_id, GJS_MODULE_PROP_FLAGS))
+            &ObjectBase::prop_setter, private_id,
+            // Make property configurable so that interface properties can be
+            // overridden by GObject.ParamSpec.override in the class that
+            // implements them
+            GJS_MODULE_PROP_FLAGS & ~JSPROP_PERMANENT))
         return false;
 
     *resolved = true;
diff --git a/installed-tests/js/testGObjectInterface.js b/installed-tests/js/testGObjectInterface.js
index 9eab9746..daefb683 100644
--- a/installed-tests/js/testGObjectInterface.js
+++ b/installed-tests/js/testGObjectInterface.js
@@ -84,6 +84,22 @@ const ImplementationOfTwoInterfaces = GObject.registerClass({
     }
 });
 
+const ImplementationOfIntrospectedInterface = GObject.registerClass({
+    Implements: [Gio.Action],
+    Properties: {
+        'enabled': GObject.ParamSpec.override('enabled', Gio.Action),
+        'name': GObject.ParamSpec.override('name', Gio.Action),
+        'state': GObject.ParamSpec.override('state', Gio.Action),
+        'state-type': GObject.ParamSpec.override('state-type', Gio.Action),
+        'parameter-type': GObject.ParamSpec.override('parameter-type',
+            Gio.Action)
+    }
+}, class ImplementationOfIntrospectedInterface extends GObject.Object {
+    get name() {
+        return 'inaction';
+    }
+});
+
 describe('GObject interface', function () {
     it('cannot be instantiated', function () {
         expect(() => new AGObjectInterface()).toThrow();
@@ -247,6 +263,11 @@ describe('GObject interface', function () {
             253, 'testGObjectMustOverrideInterfaceProperties');
     });
 
+    it('can have introspected properties overriden', function() {
+        let obj = new ImplementationOfIntrospectedInterface();
+        expect(obj.name).toEqual('inaction');
+    });
+
     it('can be implemented by a class as well as its parent class', function () {
         const SubObject = GObject.registerClass(
             class SubObject extends GObjectImplementingGObjectInterface {});
