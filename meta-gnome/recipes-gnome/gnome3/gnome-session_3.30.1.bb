DESCRIPTION = "GNOME Session"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"
DEPENDS = " \
    gtk+3 \
    glib-2.0 \
    gnome-desktop3 \
    json-glib \
    libxslt-native \
    xmlto-native \
    libx11 \
"

SRC_URI = " \
    https://download.gnome.org/core/3.30/3.30.2/sources/${PN}-${PV}.tar.xz \
    file://no-fail-on-missing-components.patch \
"

SRC_URI[md5sum] = "45c33dfaad7d40c008f8131aff2e0391"
SRC_URI[sha256sum] = "eafe85972689186c7c6b5fe1d3bb4dc204a1e0e6b6e763e24b8fb43a40c07739"

inherit pkgconfig meson gobject-introspection gettext

RDEPENDS_gnome-session += " \
    accountsservice \
    at-spi2-core \
    python3-core \
    python3-pygobject \
    gnome-settings-daemon \
    gnome-bluetooth \
    gdm \
    librsvg-gtk \
"

EXTRA_OEMESON += " -Dman=false"
EXTRA_OEMESON_remove += "-Dintrospection=true"

do_configure_prepend() {
	# Fixup the gsettings version
	sed -i 's^3.27.90^3.24.1^g' ${S}/meson.build
	sed -i "s^@PYTHON3_PATH@^${RECIPE_SYSROOT_NATIVE}/${bindir}/python3^g" ${S}/meson.build
}

FILES_${PN} += "${datadir}"

do_install_append() {
	chmod +x ${D}/usr/libexec/*
}
